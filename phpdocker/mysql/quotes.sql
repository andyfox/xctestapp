-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: mysql
-- Время создания: Янв 09 2021 г., 21:29
-- Версия сервера: 5.7.32
-- Версия PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testapp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `quotes`
--

CREATE TABLE `quotes` (
  `id` int(11) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `quote` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `quotes`
--

INSERT INTO `quotes` (`id`, `author`, `quote`) VALUES
(1, 'Kevin Kruse', 'Life isn’t about getting and having, it’s about giving and being.'),
(2, 'Napoleon Hill', 'Whatever the mind of man can conceive and believe, it can achieve.'),
(3, 'Albert Einstein', 'Strive not to be a success, but rather to be of value.'),
(4, 'Robert Frost', 'Two roads diverged in a wood, and I—I took the one less traveled by, And that has made all the difference.'),
(5, 'Florence Nightingale', 'I attribute my success to this: I never gave or took any excuse.'),
(6, 'Wayne Gretzky', 'You miss 100% of the shots you don’t take.'),
(7, 'Michael Jordan', 'I’ve missed more than 9000 shots in my career. I’ve lost almost 300 games. 26 times I’ve been trusted to take the game winning shot and missed. I’ve failed over and over and over again in my life. And that is why I succeed.'),
(8, 'Amelia Earhart', 'The most difficult thing is the decision to act, the rest is merely tenacity.'),
(9, 'Babe Ruth', 'Every strike brings me closer to the next home run.'),
(10, 'W. Clement Stone', 'Definiteness of purpose is the starting point of all achievement.'),
(11, 'Kevin Kruse', 'We must balance conspicuous consumption with conscious capitalism.'),
(12, 'John Lennon', 'Life is what happens to you while you’re busy making other plans.'),
(13, 'Earl Nightingale', 'We become what we think about.'),
(14, 'Mark Twain', 'Twenty years from now you will be more disappointed by the things that you didn’t do than by the ones you did do, so throw off the bowlines, sail away from safe harbor, catch the trade winds in your sails.  Explore, Dream, Discover.'),
(15, 'Charles Swindoll', 'Life is 10% what happens to me and 90% of how I react to it.'),
(16, 'Alice Walker', 'The most common way people give up their power is by thinking they don’t have any.'),
(17, 'Buddha', 'The mind is everything. What you think you become.'),
(18, 'Chinese Proverb', 'The best time to plant a tree was 20 years ago. The second best time is now.'),
(19, 'Socrates', 'An unexamined life is not worth living.'),
(20, 'Woody Allen', 'Eighty percent of success is showing up.'),
(21, 'Steve Jobs', 'Your time is limited, so don’t waste it living someone else’s life!'),
(22, 'Vince Lombardi', 'Winning isn’t everything, but wanting to win is.'),
(23, 'Stephen Covey', 'I am not a product of my circumstances. I am a product of my decisions.'),
(24, 'Pablo Picasso', 'Every child is an artist.  The problem is how to remain an artist once he grows up.'),
(25, 'Christopher Columbus', 'You can never cross the ocean until you have the courage to lose sight of the shore.'),
(26, 'Maya Angelou', 'I’ve learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel.'),
(27, 'Jim Rohn', 'Either you run the day, or the day runs you.'),
(28, 'Henry Ford', 'Whether you think you can or you think you can’t, you’re right.'),
(29, 'Mark Twain', 'The two most important days in your life are the day you are born and the day you find out why.'),
(30, 'Johann Wolfgang von Goethe', 'Whatever you can do, or dream you can, begin it.  Boldness has genius, power and magic in it.'),
(31, 'Frank Sinatra', 'The best revenge is massive success.'),
(32, 'Zig Ziglar', 'People often say that motivation doesn’t last. Well, neither does bathing.  That’s why we recommend it daily.'),
(33, 'Anais Nin', 'Life shrinks or expands in proportion to one’s courage.'),
(34, 'Vincent Van Gogh', 'If you hear a voice within you say “you cannot paint,” then by all means paint and that voice will be silenced.'),
(35, 'Aristotle', 'There is only one way to avoid criticism: do nothing, say nothing, and be nothing.'),
(36, 'Jesus', 'Ask and it will be given to you; search, and you will find; knock and the door will be opened for you.'),
(37, 'Ralph Waldo Emerson', 'The only person you are destined to become is the person you decide to be.'),
(38, 'Henry David Thoreau', 'Go confidently in the direction of your dreams.  Live the life you have imagined.'),
(39, 'Erma Bombeck', 'When I stand before God at the end of my life, I would hope that I would not have a single bit of talent left and could say, I used everything you gave me.'),
(40, 'Booker T. Washington', 'Few things can help an individual more than to place responsibility on him, and to let him know that you trust him.'),
(41, ' Ancient Indian Proverb', 'Certain things catch your eye, but pursue only those that capture the heart.'),
(42, 'Theodore Roosevelt', 'Believe you can and you’re halfway there.'),
(43, 'George Addair', 'Everything you’ve ever wanted is on the other side of fear.'),
(44, 'Plato', 'We can easily forgive a child who is afraid of the dark; the real tragedy of life is when men are afraid of the light.'),
(45, 'Maimonides', 'Teach thy tongue to say, “I do not know,” and thous shalt progress.'),
(46, 'Arthur Ashe', 'Start where you are. Use what you have.  Do what you can.'),
(47, 'John Lennon', 'When I was 5 years old, my mother always told me that happiness was the key to life.  When I went to school, they asked me what I wanted to be when I grew up.  I wrote down ‘happy’.  They told me I didn’t understand the assignment, and I told them they didn’t understand life.'),
(48, 'Japanese Proverb', 'Fall seven times and stand up eight.'),
(49, 'Helen Keller', 'When one door of happiness closes, another opens, but often we look so long at the closed door that we do not see the one that has been opened for us.'),
(50, 'Confucius', 'Everything has beauty, but not everyone can see.'),
(51, 'Anne Frank', 'How wonderful it is that nobody need wait a single moment before starting to improve the world.'),
(52, 'Lao Tzu', 'When I let go of what I am, I become what I might be.'),
(53, 'Maya Angelou', 'Life is not measured by the number of breaths we take, but by the moments that take our breath away.'),
(54, 'Dalai Lama', 'Happiness is not something readymade.  It comes from your own actions.'),
(55, 'Sheryl Sandberg', 'If you’re offered a seat on a rocket ship, don’t ask what seat! Just get on.'),
(56, 'Aristotle', 'First, have a definite, clear practical ideal; a goal, an objective. Second, have the necessary means to achieve your ends; wisdom, money, materials, and methods. Third, adjust all your means to that end.'),
(57, 'Latin Proverb', 'If the wind will not serve, take to the oars.'),
(58, 'Unknown', 'You can’t fall if you don’t climb.  But there’s no joy in living your whole life on the ground.'),
(59, 'Marie Curie', 'We must believe that we are gifted for something, and that this thing, at whatever cost, must be attained.'),
(60, 'Les Brown', 'Too many of us are not living our dreams because we are living our fears.'),
(61, 'Joshua J. Marine', 'Challenges are what make life interesting and overcoming them is what makes life meaningful.'),
(62, 'Booker T. Washington', 'If you want to lift yourself up, lift up someone else.'),
(63, 'Leonardo da Vinci', 'I have been impressed with the urgency of doing. Knowing is not enough; we must apply. Being willing is not enough; we must do.'),
(64, 'Jamie Paolinetti', 'Limitations live only in our minds.  But if we use our imaginations, our possibilities become limitless.'),
(65, 'Erica Jong', 'You take your life in your own hands, and what happens? A terrible thing, no one to blame.'),
(66, 'Bob Dylan', 'What’s money? A man is a success if he gets up in the morning and goes to bed at night and in between does what he wants to do.'),
(67, 'Benjamin Franklin', 'I didn’t fail the test. I just found 100 ways to do it wrong.'),
(68, 'Bill Cosby', 'In order to succeed, your desire for success should be greater than your fear of failure.'),
(69, 'Albert Einstein', 'A person who never made a mistake never tried anything new.'),
(70, 'Chinese Proverb', 'The person who says it cannot be done should not interrupt the person who is doing it.'),
(71, 'Roger Staubach', 'There are no traffic jams along the extra mile.'),
(72, 'George Eliot', 'It is never too late to be what you might have been.'),
(73, 'Oprah Winfrey', 'You become what you believe.'),
(74, 'Vincent van Gogh', 'I would rather die of passion than of boredom.'),
(75, 'Unknown', 'A truly rich man is one whose children run into his arms when his hands are empty.'),
(76, 'Ann Landers', 'It is not what you do for your children, but what you have taught them to do for themselves, that will make them successful human beings.'),
(77, 'Abigail Van Buren', 'If you want your children to turn out well, spend twice as much time with them, and half as much money.'),
(78, 'Farrah Gray', 'Build your own dreams, or someone else will hire you to build theirs.'),
(79, 'Jesse Owens', 'The battles that count aren’t the ones for gold medals. The struggles within yourself–the invisible battles inside all of us–that’s where it’s at.'),
(80, 'Sir Claus Moser', 'Education costs money.  But then so does ignorance.'),
(81, 'Rosa Parks', 'I have learned over the years that when one’s mind is made up, this diminishes fear.'),
(82, 'Confucius', 'It does not matter how slowly you go as long as you do not stop.'),
(83, 'Oprah Winfrey', 'If you look at what you have in life, you’ll always have more. If you look at what you don’t have in life, you’ll never have enough.'),
(84, 'Dalai Lama', 'Remember that not getting what you want is sometimes a wonderful stroke of luck.'),
(85, 'Maya Angelou', 'You can’t use up creativity.  The more you use, the more you have.'),
(86, 'Norman Vaughan', 'Dream big and dare to fail.'),
(87, 'Martin Luther King Jr.', 'Our lives begin to end the day we become silent about things that matter.'),
(88, 'Teddy Roosevelt', 'Do what you can, where you are, with what you have.'),
(89, 'Tony Robbins', 'If you do what you’ve always done, you’ll get what you’ve always gotten.'),
(90, 'Gloria Steinem', 'Dreaming, after all, is a form of planning.'),
(91, 'Mae Jemison', 'It’s your place in the world; it’s your life. Go on and do all you can with it, and make it the life you want to live.'),
(92, 'Beverly Sills', 'You may be disappointed if you fail, but you are doomed if you don’t try.'),
(93, 'Eleanor Roosevelt', 'Remember no one can make you feel inferior without your consent.'),
(94, 'Grandma Moses', 'Life is what we make it, always has been, always will be.'),
(95, 'Ayn Rand', 'The question isn’t who is going to let me; it’s who is going to stop me.'),
(96, 'Henry Ford', 'When everything seems to be going against you, remember that the airplane takes off against the wind, not with it.'),
(97, 'Abraham Lincoln', 'It’s not the years in your life that count. It’s the life in your years.'),
(98, 'Norman Vincent Peale', 'Change your thoughts and you change your world.'),
(99, 'Benjamin Franklin', 'Either write something worth reading or do something worth writing.'),
(100, 'Audrey Hepburn', 'Nothing is impossible, the word itself says, “I’m possible!”'),
(101, 'Steve Jobs', 'The only way to do great work is to love what you do.'),
(102, 'Zig Ziglar', 'If you can dream it, you can achieve it.');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-quotes-author` (`author`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
