<?php

use yii\db\Migration;

/**
 * Class m210109_181943_add_quotes
 */
class m210109_181943_add_quotes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('quotes', [
            'id' => $this->primaryKey(),
            'author' => $this->string(),
            'quote' => $this->text(),
        ]);

        $this->createIndex(
            'idx-quotes-author',
            'quotes',
            'author'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-quotes-author',
            'quotes'
        );

        $this->dropTable('quotes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210109_181943_add_quotes cannot be reverted.\n";

        return false;
    }
    */
}
