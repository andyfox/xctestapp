<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Quote
 * @package app\models
 *
 * @property int $id
 * @property string $author
 * @property string $quote
 */
class Quote extends ActiveRecord
{
    protected $fillable = [
        'author',
        'quote',
    ];

    public static function tableName()
    {
        return '{{quotes}}';
    }

}
