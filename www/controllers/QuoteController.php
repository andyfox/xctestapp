<?php


namespace app\controllers;

use app\traits\Cacheable;
use app\jobs\SetCacheJob;
use app\services\QuoteService;
use yii\rest\Controller;

class QuoteController extends Controller
{
    use Cacheable;

    public function actionShout()
    {
        $request = \Yii::$app->request;
        $key = $this->getKeyByRequest($request);

        if ($items = $this->cacheGet($key)) {
            $this->asJson($items);
        }

        $author = $request->get('author');
        $limit = $request->get('limit');
        $quoteService = new QuoteService();
        $items = $quoteService->getQuotes($author, $limit);

        \Yii::$app->queue->push(new SetCacheJob([
            'key' => $key,
            'items' => $items
        ]));

        return $this->asJson($items);
    }

    public function actionTest()
    {
        $request = \Yii::$app->request;
        $key = $this->getKeyByRequest($request);

        if ($items = $this->cacheGet($key)) {
            $this->asJson($items);
        }
    }
}