<?php


namespace app\dto;


use yii\base\BaseObject;

class ErrorObject extends BaseObject
{
    /** @var array */
    public $errors;
}