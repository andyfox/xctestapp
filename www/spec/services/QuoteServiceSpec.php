<?php

namespace spec\app\services;

use PhpSpec\ObjectBehavior;
use app\services\QuoteService;

class QuoteServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(QuoteService::class);
    }

    function it_gets_quotes()
    {
        $this->getQuotes()->shouldBeArray();
    }
}
