<?php


namespace app\traits;

use yii\web\Request;

trait Cacheable
{
    private $duration = 10; // seconds

    /**
     * @param $key
     * @return mixed
     */
    public function cacheGet($key)
    {
        return  \Yii::$app->cache->get($key);
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function cacheSet($key, $value)
    {
        return \Yii::$app->cache->set($key, $value, $this->duration);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getKeyByRequest($request)
    {
        return $request->queryString;
    }
}