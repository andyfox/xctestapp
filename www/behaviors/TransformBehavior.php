<?php


namespace app\behaviors;

use yii\base\Behavior;

class TransformBehavior extends Behavior
{
    /**
     * @param array $toShout
     * @return array
     */
    public function shout($toShout)
    {
        foreach ($toShout as &$string) {
            $string = rtrim($string, '.');
            $string = strtoupper($string) . '!';
        }

        return $toShout;
    }
}