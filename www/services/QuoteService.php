<?php

namespace app\services;


use app\behaviors\TransformBehavior;
use app\dto\ErrorObject;
use app\models\Quote;
use yii\base\Component;
use yii\base\DynamicModel;

class QuoteService extends Component
{
    const QUOTES_LIMIT = 10;

    public function behaviors()
    {
        return [
            TransformBehavior::class
        ];
    }

    /**
     * @param string|null $author
     * @param int $limit
     * @return ErrorObject|Quote[]|array|\yii\db\ActiveRecord[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getQuotes($author = null, $limit = 1)
    {
        $model = DynamicModel::validateData(compact('author', 'limit'), [
            ['author', 'trim'],
            ['limit', 'integer', 'max' => self::QUOTES_LIMIT],
        ]);

        if (!$model->validate()) {
            $errorObject = new ErrorObject();
            $errorObject->errors = $model->errors;
            return $errorObject;
        }

        $quotes = Quote::find()->select('quote')->where(['author' => $author])->limit($limit)->column();
        $quotes = $this->shout($quotes);


        return $quotes;
    }
}