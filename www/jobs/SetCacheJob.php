<?php


namespace app\jobs;

use app\traits\Cacheable;
use yii\base\BaseObject;
use yii\queue\Job;

class SetCacheJob extends BaseObject implements Job
{
    public $key;
    public $items;

    use Cacheable;

    public function execute($queue)
    {
        $this->cacheSet($this->key, $this->items);
    }
}