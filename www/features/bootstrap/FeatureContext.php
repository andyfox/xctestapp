<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * @var \GuzzleHttp\Psr7\Response
     */
    private $response;

    /**
     * @var GuzzleHttp\Client
     */
    private $client;

    /**
     * @var string
     */
    private $uri;

    /**
     * @var array
     */
    private $list;

    /**
     * @Given I am on :arg1
     * @param $arg1
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws Exception
     */
    public function iAmOn($arg1)
    {
        $this->uri = '/quote/shout';
        $this->client = new GuzzleHttp\Client(['base_uri' => $arg1]);
        $this->response = $this->client->get($this->uri);

        if ($this->response->getStatusCode() != 200) {
            throw new Exception("The endpoint is not available");
        }

        return true;
    }

    /**
     * @When I pass :arg1 parameter with value :arg2
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function iPassParameterWithValue($arg1, $arg2)
    {
        $this->response = $this->client->get($this->uri, [
            $arg1 => $arg2
        ]);

        $this->list = json_decode($this->response->getBody());

        if ($arg1 == 'limit') {
            if (count($this->list) > $arg2) {
                throw new Exception("Limit parameter has no effect");
            }
        }

        return true;
    }

    /**
     * @Then I should see the list
     * @throws Exception
     */
    public function iShouldSeeTheList()
    {
        if (!is_array($this->list)) {
            throw new Exception("Incorrect API answer");
        }

        return true;
    }

}
